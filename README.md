# ROBot.py
## ABOUT
This is a bot written in python made to log all activity on Discord servers. It writes all data to a MySQL database. It uses the [Discord.Py](https://github.com/Rapptz/discord.py) wrapper to communicate with the Discord API. It comes with a web interface that i'm currently still writing.

You can find the web interface [here](https://gitlab.com/RoobieBoobieee/Discord_Log_Bot_Web).

## INSTALL
Assuming you already have MySQL set up. Make an empty database, and enter the name in the config as explained below. These are the installation steps for an Ubuntu system.


### CLONE BOT
```
$ git clone https://gitlab.com/RoobieBoobieee/Discord_Bot.git
```

#### INSTALL Python 3.5
```
$ sudo add-apt-repository ppa:fkrull/deadsnakes

$ sudo apt-get update

$ sudo apt-get install -y python3.5 python3-setuptools python3-pip
```

#### INSTALL PyMySQL
Download the source code from [the website](https://pypi.python.org/pypi/PyMySQL).
Currently this is version 0.7.9.
If PyMySQL gets updated, you'll have to edit the commands below to the new version number.
```
$ wget https://pypi.python.org/packages/a4/c4/c15457f261fda9839637de044eca9b6da8f55503183fe887523801b85701/PyMySQL-0.7.9.tar.gz#md5=bf82311ac2df4c43adad003a8c805b90

$ tar -zxvf PyMySQL-0.7.9.tar.gz

$ cd PyMySQL-0.7.9

$ python3.5 setup.py build

$ sudo python3.5 setup.py install
```
#### INSTALL Discord.py
Install  Discord.Py wrapper.
```
$ sudo python3.5 -m pip install -U discord.py
```

## CONFIGURATION
Make a copy of the the `config.py.example` file and rename it to `config.py`

```
$ cp config.py.example config.py
```

Enter your bot token and database credentials

If you enter your discord id in the owner variable, you will get notifications on discord about new members, bans and newly assigned roles.

## RUN
* Simply run the command:

```
$ python3.5 ROBot.py
```
When you first run the bot, it will create all tables so it could take a while to start.

## NOTE
This bot hasn't been subject of heavy testing. All issues are welcome, i'm happy to help :)

## EXTRA
These are the tables the bot will create:

* channels
* members
* messages
* servers
* servers_members
* member_changes
* invites
* roles
* permissions
* role_permissions
* member_roles
* emojis
* emoji_roles
* server_changes
* channel_changes
* role_changes
* voice_changes
* reactions
* private_channels
* private_channels_recipients
